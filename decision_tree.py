__author__ = "Joe Sarsfield"

import math
import pandas as pd
import sys
from graphviz import Digraph


data = None  # Store all the data
data_headings = None  # Store attribute and target headings for visualisation
tree = {"nodes": [{"Parent": None, "bIsLeaf": False, "Children": []}]}  # Store the trained tree data


def train_decision_tree():
    global data
    node_evaluating = 0
    tree["nodes"][0]["data"] = data.copy(True) # Deep copy
    while node_evaluating < len(tree["nodes"]):
        # Test if leaf node or only one attribute
        if tree["nodes"][node_evaluating]["bIsLeaf"] == True:
            node_evaluating += 1  # Move to next node
            continue
        ndata = tree["nodes"][node_evaluating]["data"]  # Node data
        # Calculate Target entropy
        target_entropy = entropy(ndata["T"])
        # Find attribute with highest information gain
        entropies = []
        IG = []
        highest_ig = -1 # Highest IG
        best_attrib = None  # Attribute with highest IG
        attributes = []
        for A in ndata.filter(like="A"):
            values = ndata[A].value_counts()
            attrib_entropy = 0
            for ind, V in enumerate(values):
                prob = V/sum(values)
                entr = entropy(ndata.loc[ndata[A] == values.index[ind], "T"])
                attrib_entropy += prob*entr
            entropies.append(attrib_entropy)
            ig = target_entropy-attrib_entropy
            if ig > highest_ig:
                highest_ig = ig
                best_attrib = A
            IG.append(ig)
            attributes.append(data_headings[int(A[1:])])
        # Update current node
        update_node(node_evaluating, target_entropy, IG, attributes, best_attrib, highest_ig)
        # Split dataset and add nodes
        values = ndata[best_attrib].value_counts()
        for ind, V in enumerate(values):
            child_data = ndata.loc[ndata[best_attrib] == values.index[ind]].copy(True)
            child_data = child_data.drop([best_attrib], axis=1)  # Remove best attribute as its already in the tree
            child_data.index = range(len(child_data)) # Reindex from 0
            bIsLeaf = False
            # Test if child is leaf node as Target is the same or there is only one attribute
            if len(child_data["T"].value_counts()) == 1 or len(child_data.filter(like="A").columns) == 0:
                bIsLeaf = True
            child_ind = add_node(child_data, node_evaluating, values.index[ind], bIsLeaf)  # Add child node
            if bIsLeaf:
                tree["nodes"][child_ind]["Output"] = data_headings[-1] + " = " + child_data["T"].value_counts().idxmax()
            add_child(node_evaluating, child_ind)  # Add child to current parent node
        node_evaluating += 1  # Move to next node


def load_file(fn):
    global data, data_headings
    if fn[-4:] != ".csv":
        fn += ".csv"
    data = pd.read_csv(fn, header=None, index_col=None)
    data.columns = ["A"+str(ind) if ind != (len(data.columns)-1) else "T" for ind, col in enumerate(data)] # Give columns "A" attribute and "T" target column headers
    data_headings = list(data.loc[0])
    data = data.drop(0) # Remove headings from data
    data.index = range(len(data)) # Reindex from 0


def update_node(node_ind, T_entropy, IGs, attributes, split_attrib, highest_ig):
    tree["nodes"][node_ind]["T_Entropy"] = T_entropy
    tree["nodes"][node_ind]["IGs"] = IGs
    tree["nodes"][node_ind]["attributes"] = attributes
    tree["nodes"][node_ind]["Split_Attrib"] = split_attrib
    tree["nodes"][node_ind]["highest_ig"] = highest_ig


def add_node(node_data, parent, split_value, bIsLeaf=False):  # Add node to tree
    global tree
    tree["nodes"].append({"data": node_data, "Split_Value": split_value, "bIsLeaf": bIsLeaf, "Parent": parent, "Children": []})
    return len(tree["nodes"])-1


def add_child(ind, child_ind):  # Add child node
    tree["nodes"][ind]["Children"].append(child_ind)


def entropy(d):
    res = 0
    values = d.value_counts()
    total = sum(values)
    for val in values:
        if val == 0:
            continue
        valNorm = val/total
        res -= valNorm*math.log(valNorm, 2)
    return res


def draw_decision_tree():
    global tree
    dot = Digraph(name='Decision Tree Predicting ' + data_headings[-1], node_attr={'shape': 'box'})
    dot.attr(label='Decision Tree Predicting ' + data_headings[-1], labelloc = "t")
    for ind, node in enumerate(tree["nodes"]):
        if node["bIsLeaf"]:
            dot.node(str(ind), node["Output"])
        else:
            output = "<"
            for i, attrib in enumerate(node["attributes"]):
                if attrib == data_headings[int(node["Split_Attrib"][1:])]:
                    output += "<B>"+ attrib + " IG: " + str(node["IGs"][i]) + "</B><br/>"
                else:
                    output += attrib + " IG: " +str(node["IGs"][i])+"<br/>"
            output += ">"
            dot.node(str(ind), output)
        if node["Parent"] is not None:
            dot.edge(str(node["Parent"]), str(ind), label=node["Split_Value"])

    dot.render("output/decision-tree-"+data_headings[-1]+".gv", view=True)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    else:
        filename = "Dataset.csv"
    load_file(filename)
    train_decision_tree()
    draw_decision_tree()